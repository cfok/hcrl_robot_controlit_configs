hcrl_robot_controlit_configs_dir=`rosstack find hcrl_robot_controlit_configs`
config_directories=`find $hcrl_robot_controlit_configs_dir -mindepth 1 -maxdepth 1 -type d \( ! -iname ".git" \)`

for d in $config_directories; do
    export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$d/models
    export GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:$d/lib
done
