#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p dreamer_controlit/urdf

echo Generating dreamer_controlit/urdf/dreamer_gazebo.urdf
rosrun xacro xacro.py dreamer_controlit/xacro/dreamer_gazebo.xacro -o dreamer_controlit/urdf/dreamer_gazebo.urdf

echo Generating dreamer_controlit/urdf/dreamer_controlit.urdf
rosrun xacro xacro.py dreamer_controlit/xacro/dreamer_controlit.xacro -o dreamer_controlit/urdf/dreamer_controlit.urdf

echo Done!